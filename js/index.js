var ratings = [0, 0, 0, 0];

$(document).ready(function(){

  $(':radio').change(function(){
    var val = this.value;
    var ratingId = $(this).parent().parent().attr('id');
    if (ratingId == 'rating1')
      ratings[0] = val;
    if (ratingId == 'rating2')
      ratings[1] = val;
    if (ratingId == 'rating3')
      ratings[2] = val;
    if (ratingId == 'rating4')
      ratings[3] = val;
  });

  $("#submitButton").click(function(){
    var form = $("#ratingForm");
    var userName = $("#userName").val();
    var userEmail = $("#userEmail").val();
    var userPhone = $("#userPhone").val();
    var comments = $("#userComments").val();

    console.log("Rating1:" + ratings[0]);
    console.log("Rating2:" + ratings[1]);
    console.log("Rating3:" + ratings[2]);
    console.log("Rating4:" + ratings[3]);

    console.log(userName);
    console.log(userEmail);
    console.log(userPhone);
    console.log(comments);

    if (form.valid())
    {
      console.log("Form is valid");
      var reviewData = {
        uName: userName,
        uEmail: userEmail,
        uPhone: userPhone,
        uComments: comments,
        r1: ratings[0],
        r2: ratings[1],
        r3: ratings[2],
        r4: ratings[3]
      };
      var reviewStr = [reviewData];
      reviewStr = JSON.stringify(reviewStr);

      $.ajax({
        url: 'https://script.google.com/macros/s/AKfycbyW-KjEVsv5oDSe_N-2jHdCRVtaokQ-yinywaTogYw3Lb_YZCQ/exec', // your api url
        method: 'POST',
        // headers: {
        //   'Content-Type':'application/json'
        // },
        data: reviewStr,
        success: function(){
          // console.log(data);
          // console.log(status);
        }
      });
      window.location.href = "thankyou.html";
    }
    else
    {
      console.log("Form is invalid");
      alert("Please enter valid inputs.");
    }
  });
});
